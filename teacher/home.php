<?php
session_start();
ob_start();
$t_id_card = $_SESSION['t_id_card'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>
    <div style="display: flex;flex-direction: column;align-items: center;  gap: 20px;">
        <div style="display: flex;gap: 30px;">
            <a href="fi_webboard.php" class="btn btn-dark btn-lg">สร้างกระทู้</a>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="row align-items-start">
            <div class="col">
                <!-- เข้าสู่ระบบ -->
                <table class="table table-primary">
                    <thead>
                        <tr align="center">
                            <th scope="col" colspan="7">
                                <font face="boonjot" size="5">เว็บบอร์ด</font>
                            </th>
                        </tr>
                    </thead>

                    <tbody class="table-group-divider">
                        <tr align="center table-info" class="table-info">
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ลำดับ</font>
                            </td>

                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">รูปภาพ</font>
                            </td>

                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">หัวข้อกระทู้</font>
                            </td>

                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ผู้โพสต์</font>
                            </td>

                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">วันที่โพสต์</font>
                            </td>

                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ยอดวิว</font>
                            </td>

                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ความคิดเห็น</font>
                            </td>

                        </tr>
                        <!-- ตัวแปรแสดงข้อมูลจากฐานข้อมูล -->
                        <?php include ("connect_db.php");
                        $sql = "select * from tb_webboard";
                        $rs = $conn->query($sql);
                        $no = 1;
                        while ($r = $rs->fetch_object()) {
                            ?>
                            <tr align="center table-info" class="table-info">
                                <td align="center" class="col-sm">
                                    <font face="boonjot" size="4"><?= $no++ ?></font>
                                </td>

                                <td align="center" class="col-sm">
                                    <font face="boonjot" size="4"><img src="../img/webboard/<?= $r->w_img ?>" alt=""
                                            width="50px"></font>
                                </td>

                                <td align="center" class="col-sm">
                                    <font face="boonjot" size="4">
                                        <a href="topic_detail.php?w_id=<?= $r->w_id ?>">
                                            <?= $r->w_topic ?>
                                        </a>
                                    </font>
                                </td>

                                <td align="center" class="col-sm">
                                    <font face="boonjot" size="4"><?= $r->w_user ?></font>
                                </td>

                                <td align="center" class="col-sm">
                                    <font face="boonjot" size="4"><?= $r->w_date ?></font>
                                </td>

                                <td align="center" class="col-sm">
                                    <font face="boonjot" size="4"><?= $r->w_view ?></font>
                                </td>

                                <td align="center" class="col-sm">
                                    <font face="boonjot" size="4"><?= $r->w_comment ?></font>
                                </td>

                            </tr>
                        <?php } ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>