<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom Syetem</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>

<body style="background-color: #508bfc;">

    <section style="background-color: #508bfc;">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card shadow-2-strong" style="border-radius: 1rem;">
                        <div class="card-body p-3 text-center">


                            <h3 class="mb-4">ลงทะเบียน คุณครู</h3>
                            <form name="user_register" action="register_teacher.php" method="post"
                                enctype="multipart/form-data">

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">ชื่อผู้ใช้งาน</span>
                                    <input type="text" class="form-control" name="t_id_card"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required placeholder="รหัสบัตรประจำตัวประชาชน">
                                </div>

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">ชื่อนาม-สกุล</span>
                                    <input type="text" class="form-control" name="t_name"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">วันเกิด</span>
                                    <input type="date" class="form-control" name="t_date_of_birth"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>

                                <div class="input-group mb-3"
                                    style="display;gap: 10px;align-items: center;justify-content: space-between;padding-right: 160px;border: 1px solid #dee2e6;border-radius: 8px;">
                                    <span class="input-group-text" id="inputGroup-sizing-default">
                                        เพศ
                                    </span>
                                    <div style="display: flex; gap: 10px;">
                                        <input type="radio" class="" name="t_gender" value="ชาย" required>ชาย
                                        <input type="radio" class="" name="t_gender" value="หญิง" required>หญิง
                                    </div>
                                </div>

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">ที่อยู่</span>
                                    <input type="text" class="form-control" name="t_address"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">เบอร์โทรศัพท์</span>
                                    <input type="text" class="form-control" name="t_tel"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">อีเมล</span>
                                    <input type="email" class="form-control" name="t_email"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">วุฒิการศึกษา </span>
                                    <input type="text" class="form-control" name="t_degree"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">ตำแหน่งงาน </span>
                                    <input type="text" class="form-control" name="t_position"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">วันเริ่มงาน</span>
                                    <input type="date" class="form-control" name="t_date_of_hire"
                                        aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                        required>
                                </div>

                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-default">รูปภาพของคุณ
                                        </spanrequired>
                                        <input type="file" class="form-control" name="t_img"
                                            aria-label="Sizing example input"
                                            aria-describedby="inputGroup-sizing-default" required>
                                </div>

                                <button data-mdb-button-init data-mdb-ripple-init
                                    class="btn btn-primary btn-lg btn-block" type="submit">
                                    ลงทะเบียน
                                </button>
                                <button data-mdb-button-init data-mdb-ripple-init
                                    class="btn btn-danger btn-lg btn-block" type="reset">
                                    ยกเลิก
                                </button>
                            </form>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
        crossorigin="anonymous"></script>
</body>

</html>