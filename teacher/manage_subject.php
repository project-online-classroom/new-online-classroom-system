<?php
session_start();
ob_start();
$t_id_card = $_SESSION['t_id_card'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>
    <center>
        <div class="container" style="margin-bottom: 20px; width: 60%;">
        <center>
        <h4>จัดการรายวิชา</h4>
    </center>
            <a href="form_insert_class.php" class="btn btn-light btn-lg" style="margin-bottom: 10px; margin-top: 10px;">สร้างรายวิชา</a>
            <form class="d-flex" action="" method="post">
                <input class="form-control me-2" type="search" name="txt_s" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </center>
    <div class="container text-center" style="display: flex; flex-wrap: wrap; ">
        <?php
        include ("connect_db.php");
        $txt_s = $_POST['txt_s'];
        $sql = "select * from tb_subject inner join tb_category on tb_subject.c_id = tb_category.c_id
        where tb_subject.t_id_card = '$t_id_card' and (subj_id like '%" . $txt_s . "%' or subj_name like '%" . $txt_s . "%' or tb_subject.c_id like '%" . $txt_s . "%' )
        order by subj_id asc";
        $rs = $conn->query($sql);
        while ($r = $rs->fetch_object()) {
            ?>
            <div class="col-md-3" style="margin-bottom: 20px;">
                <div class="card" style="width: 18rem;">
                    <img src="../img/subject/<?= $r->subj_img; ?>" class="card-img-top" alt="..."
                        style="width: 280px; height: 200px;">
                    <div class="card-body">
                        <h5 class="card-title">รหัสวิชา : <?= $r->subj_id; ?></h5>
                        <p class="card-text">ชื่อวิชา : <?= $r->subj_name; ?></p>
                        <p class="card-text">ชื่อหมวดหมู่ : <?= $r->c_name; ?></p>
                        <a href="subject.php?subj_id=<?=$r->subj_id ;?>" class="btn btn-primary">ดูราชวิชา</a>
                        <a href=".php?c_id=<?=$r->c_id?>" class="btn btn-warning">แก้ไข</a>
                        <a href=".php?c_id=<?=$r->c_id;?>" class="btn btn-danger">ลบ</a>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>