<?php
session_start();
ob_start();
$t_id_card = $_SESSION['t_id_card'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<style>
    .main-container {
        width: 600px;
        display: flex;
        gap: 30px;
        border: 1px solid;
        border-radius: 5px;
        padding: 10px;
        margin-bottom: 20px;
    }
</style>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <?php include ("connect_db.php");
    $subj_id = $_GET['subj_id'];
    $sql = "select * from tb_teacher inner join tb_subject on tb_teacher.t_id_card = tb_subject.t_id_card 
            where tb_teacher.t_id_card = '$t_id_card' and tb_subject.subj_id = '$subj_id'";
    $rs = $conn->query($sql);
    $r = $rs->fetch_object();
    ?>

    <div class="container text-center main-container">
        <div>
            <img src="img/profile/<?= $r->t_img; ?>" alt="" style="height: 200px; width: 200px; border-radius: 50%;">
        </div>
        <div style="display: flex;flex-direction: column; align-items: flex-start; gap: 10px; justify-content: center;">
            <div>ชื่อรายวิชา : <?= $r->subj_name ?></div>
            <div>รหัสวิชา : <?= $r->subj_id ?></div>
            <div>ชื่อครูผู้สอน : <?= $r->t_name ?></div>
            <div>เบอร์ครูผู้สอน : <?= $r->t_tel ?></div>
        </div>
    </div>

    <div class="container text-center main-container" style="justify-content: center;">
        <a href="student_inclass.php?subj_id=<?=$subj_id ;?>" class="btn btn-outline-success">นักเรียนในห้องเรียนและนักเรียนที่รออนุมัติ</a>
        <a href="form_insert_lesson.php?subj_id=<?=$subj_id ;?>" class="btn btn-outline-success">สร้างบทเรียน</a>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>