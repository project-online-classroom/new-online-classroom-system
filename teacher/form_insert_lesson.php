<?php
session_start();
ob_start();
$t_id_card = $_SESSION['t_id_card'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <center>
        <h4>สร้างบทเรียน</h4>
    </center>
    
    <div class="container text-center">
        <div style="display: flex; justify-content: center;">
            <div class="col-6">
                <form action="insert_lesson.php" method="post" enctype="multipart/form-data">
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">ชื่อบทเรียน</span>
                        <input type="text" class="form-control" name="ls_name" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" required>
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">รายละเอียดบทเรียน</span>
                        <textarea class="form-control" name="ls_des" id="floatingTextarea2"
                            style="height: 125px"><?= $r->w_detail; ?></textarea>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">แนบลิ้ง</span>
                        <input type="url" id="ls_link" name="ls_link" >
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">ไฟล์งาน</span>
                        <input type="file" class="form-control" name="ls_file" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" >
                    </div>

                    <button type="submit" class="btn btn-success">สร้างบทเรียน</button>
                    <button type="reset" class="btn btn-warning">ยกเลิก</button>
                    <input type="hidden" name="subj_id" value="<?= $_GET['subj_id']; ?>">
                </form>
            </div>

        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>