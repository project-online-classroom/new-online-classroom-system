<?php
include ("connect_db.php");

$t_id_card = $_POST['t_id_card'];
$t_name = $_POST['t_name'];
$t_date_of_birth = $_POST['t_date_of_birth'];
$t_gender = $_POST['t_gender'];
$t_address = $_POST['t_address'];
$t_tel = $_POST['t_tel'];
$t_email = $_POST['t_email'];
$t_degree = $_POST['t_degree'];
$t_position = $_POST['t_position'];
$t_date_of_hire = $_POST['t_date_of_hire'];

$t_img = $_FILES['t_img']['name'];
$t_img_path = $_FILES['t_img']['tmp_name'];

if ($t_img == '') {
    $sql = "update tb_teacher set
       t_id_card ='$t_id_card',
t_name = '$t_name',
t_date_of_birth = '$t_date_of_birth',
t_gender = '$t_gender',
t_address = '$t_address',
t_tel = '$t_tel',
t_email = '$t_email',
t_degree ='$t_degree',
t_position = '$t_position',
t_date_of_hire = '$t_date_of_hire' 
        where t_id_card = '$t_id_card'";
    $rs = $conn->query($sql);
    if ($rs) {
        ?>
        <script language="javascript">
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location = "profile.php";
        </script>
        <?php
    } else {
        echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
        echo $sql;
        exit();
    }
} else {
    // ลบรูปเก่าออก
    $old_img_sql = "SELECT t_img FROM tb_teacher WHERE t_id_card = '$t_id_card'";
    $old_img_result = $conn->query($old_img_sql);
    $old_img_row = $old_img_result->fetch_assoc();
    $old_img_path = "img/profile/" . $old_img_row['t_img'];
    unlink($old_img_path);

    $sql = "update tb_teacher set
     t_id_card ='$t_id_card',
t_name = '$t_name',
t_date_of_birth = '$t_date_of_birth',
t_gender = '$t_gender',
t_address = '$t_address',
t_tel = '$t_tel',
t_email = '$t_email',
t_degree ='$t_degree',
t_position = '$t_position',
t_date_of_hire = '$t_date_of_hire',
t_img = '$t_img' 
        where t_id_card = '$t_id_card'";
    $rs = $conn->query($sql);
    if ($rs) {
        move_uploaded_file($t_img_path, "img/profile/" . $t_img);
        ?>
        <script language="javascript">
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location = "profile.php";
        </script>
        <?php
    } else {
        echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
        echo $sql;
        exit();
    }
}
?>