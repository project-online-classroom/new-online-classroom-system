<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom Syetem</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>

<body>

    <section class="vh-100 bg-dark">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card shadow-2-strong" style="border-radius: 1rem;">
                        <div class="card-body p-5 text-center"
                            style="display: flex; gap: 10px; justify-content: center;">
                            <!-- โค้ด modal -->
                            <div class="modal fade" id="exampleModalToggle" aria-hidden="true"
                                aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalToggleLabel">เข้าสู่ระบบ</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <a href="student/index.php" class="btn btn-lg btn-outline-primary">เข้าสู่ระบบระบบสำหรับ
                                                <b>นักเรียน</b>
                                            </a>
                                            <a href="teacher/index.php" class="btn btn-lg btn-outline-primary">เข้าสู่ระบบระบบสำหรับ
                                                <b>คุณครู</b>
                                            </a>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" data-bs-target="#exampleModalToggle2"
                                                data-bs-toggle="modal">ลงทะเบียน</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModalToggle2" aria-hidden="true"
                                aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered  modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">ลงทะเบียน</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <a href="student/form_register.php"
                                                class="btn btn-lg btn-outline-info">ลงทะเบียนระบบสำหรับ <b>นักเรียน</b>
                                            </a>
                                            <a href="teacher/form_register.php"
                                                class="btn btn-lg btn-outline-info">ลงทะเบียนระบบสำหรับ <b>คุณครู</b>
                                            </a>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-bs-target="#exampleModalToggle"
                                                data-bs-toggle="modal">เข้าสู่ระบบ</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-primary" data-bs-target="#exampleModalToggle"
                                data-bs-toggle="modal">เข้าสู่ระบบ / ลงทะเบียน</button>
                            <!-- สิ้นสุดโค้ด modal -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
        crossorigin="anonymous"></script>
</body>

</html>