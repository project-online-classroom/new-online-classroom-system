<?php
session_start();
ob_start();
$s_user = $_SESSION['s_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<style>
    .main-container {
        width: 600px;
    }
</style>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>
    <div class="container text-center main-container" style="margin-bottom: 20px;">
        <button onclick="history.back()" class="btn btn-warning">กลับไปยังหน้า ห้องเรียน</button>
    </div>
    <!-- นักเรียนในห้องเรียน -->
    <div class="container text-center main-container" style="justify-content: center;">
        <h3><b>นักเรียนในห้องเรียน</b></h3>
        <hr>
    </div>
    <div class="container text-center main-container">
        <table class="table table-success">
            <tbody>
                <?php include ("connect_db.php");
                $subj_id = $_GET['subj_id'];
                $sql = "select * from tb_apjoin inner join tb_student on tb_apjoin.s_user = tb_student.s_user
                        where subj_id = '$subj_id' and aj_status = '2' order by s_name asc";
                $rs = $conn->query($sql);
                $no = 1;
                while ($r = $rs->fetch_object()) {
                    ?>
                    <tr>
                        <td scope="row">
                            <font size="4"><?= $no++ . '. ' . $r->s_name; ?></font>
                        </td>
                        <td scope="row"><a href="delete_std_inclass.php?s_user=<?= $r->s_user; ?>&subj_id=<?= $subj_id; ?>"
                                class="btn btn-outline-danger btn-sm">ออก</a></td> 
                    </tr>
                    <!-- ยังไม่เสร็จต้องลบข้อมูลหรืองานที่ส่งของนักเรียนคนนี้ด้วย -->
                <?php } ?>
            </tbody>
        </table>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>