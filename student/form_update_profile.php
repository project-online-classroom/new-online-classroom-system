<?php
session_start();
ob_start();
$s_user = $_SESSION['s_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<style>
    .main-container {
        width: 600px;
        display: flex;
        flex-direction: column;
        border: 1px solid;
        border-radius: 5px;
        padding: 10px;
        margin-bottom: 30px;
    }
</style>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <?php include ("connect_db.php");
    $sql = "select * from tb_student where s_user = '$s_user'";
    $rs = $conn->query($sql);
    $r = $rs->fetch_object();
    ?>

    <div class="container text-center main-container">
        <div style="margin-bottom: 10px">
            <h3>แบบฟอร์ม แก้ไขข้อมูล</h3>
        </div>

        <?php
        include ("connect_db.php");
        $s_id = $_GET['s_id'];
        $sql = "select * from tb_student where s_id = '$s_id'";
        $rs = $conn->query($sql);
        $r = $rs->fetch_object();
        ?>

        <form name="user_update_profile" action="update_profile.php" method="post" enctype="multipart/form-data">
            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รหัสนักเรียน</span>
                <input type="text" class="form-control" name="s_id" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_id; ?>" disabled required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ชื่อผู้ใช้งาน</span>
                <input type="text" class="form-control" name="s_user" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_user; ?>" disabled required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รหัสผ่านผู้ใช้งาน</span>
                <input type="password" class="form-control" name="s_pass" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_pass; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ชื่อนาม-สกุล</span>
                <input type="text" class="form-control" name="s_name" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_name; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">วันเกิด</span>
                <input type="date" class="form-control" name="s_date_of_birth" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_date_of_birth; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ที่อยู่</span>
                <input type="text" class="form-control" name="s_address" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_address; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">เบอร์โทรศัพท์</span>
                <input type="text" class="form-control" name="s_tel" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_tel; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">อีเมล</span>
                <input type="email" class="form-control" name="s_email" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->s_email; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รูปภาพของคุณ :
                    </spanrequired>
                    <input type="file" class="form-control" name="s_img" aria-label="Sizing example input"
                        aria-describedby="inputGroup-sizing-default">
            </div>

            <button class="btn btn-warning btn-block" type="submit">แก้ไขข้อมูล</button>
            <button type="reset" class="btn btn-danger">ยกเลิก</button>
            <input type="hidden" name="s_user" value="<?= $s_user; ?>">
        </form>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>