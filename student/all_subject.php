<?php
session_start();
ob_start();
$s_user = $_SESSION['s_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>
    <center>
        <div class="container" style="margin-bottom: 20px; width: 60%;">
            <center>
                <h4>รายวิชา</h4>
            </center>
            <form class="d-flex" action="" method="post">
                <input class="form-control me-2" type="search" name="txt_s" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </center>
    <div class="container text-center" style="display: flex; flex-wrap: wrap; ">
        <?php
        include ("connect_db.php");
        $txt_s = $_POST['txt_s'];
        $sql = "select * from tb_subject inner join tb_category on tb_subject.c_id = tb_category.c_id
        where tb_subject.subj_id like '%" . $txt_s . "%' or tb_subject.subj_name like '%" . $txt_s . "%' or tb_subject.c_id like '%" . $txt_s . "%'
        order by tb_subject.subj_id asc";
        $rs = $conn->query($sql);
        while ($r = $rs->fetch_object()) {
            ?>
            <div class="col-md-3" style="margin-bottom: 20px;">
                <div class="card" style="width: 18rem;">
                    <img src="../img/subject/<?= $r->subj_img; ?>" class="card-img-top" alt="..."
                        style="width: 280px; height: 200px;">
                    <div class="card-body">
                        <h5 class="card-title">รหัสวิชา : <?= $r->subj_id; ?></h5>
                        <p class="card-text">ชื่อวิชา : <?= $r->subj_name; ?></p>
                        <p class="card-text">ชื่อหมวดหมู่ : <?= $r->c_name; ?></p>
                        <?php
                        include ("connect_db.php");
                        $sql_aj = "select * from tb_apjoin where s_user = '$s_user' and subj_id = '$r->subj_id'";
                        $rs_aj = $conn->query($sql_aj);
                        $r_aj = $rs_aj->fetch_object();
                        if ($r_aj->aj_status == 0) { ?>
                            <a href="joinclass.php?subj_id=<?= $r->subj_id; ?>"
                                class="btn btn-primary">เข้าร่วมห้องเรียน</a>
                        <?php } else if($r_aj->aj_status == 1) { ?>
                            <a href="" class="btn btn-primary">รอการอนุมัติ</a>
                        <?php } else { 
                            ?>
                            <a href="subject.php?subj_id=<?= $r->subj_id; ?>&t_id_card=<?= $r->t_id_card; ?>"
                                    class="btn btn-primary">ดูห้องเรียน</a>
                        <?php } ?>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>