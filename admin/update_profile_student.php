<?php
include ("connect_db.php");

$s_user = $_POST['s_user'];
$s_pass = $_POST['s_pass'];
$s_name = $_POST['s_name'];
$s_date_of_birth = $_POST['s_date_of_birth'];
$s_address = $_POST['s_address'];
$s_tel = $_POST['s_tel'];
$s_email = $_POST['s_email'];
$s_img = $_FILES['s_img']['name'];
$s_img_path = $_FILES['s_img']['tmp_name'];

if ($s_img == '') {
    $sql = "update tb_student set
        s_pass = '$s_pass',
        s_name = '$s_name',
        s_date_of_birth = '$s_date_of_birth',
        s_address = '$s_address',
        s_tel = '$s_tel',
        s_email = '$s_email'
        where s_user = '$s_user'";
    $rs = $conn->query($sql);
    if ($rs) {
        ?>
        <script language="javascript">
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location = "student_acc.php";
        </script>
        <?php
    } else {
        echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
        echo $sql;
        exit();
    }
} else {
    // ลบรูปเก่าออก
    $old_img_sql = "SELECT s_img FROM tb_student WHERE s_user = '$s_user'";
    $old_img_result = $conn->query($old_img_sql);
    $old_img_row = $old_img_result->fetch_assoc();
    $old_img_path = "../student/img/profile/" . $old_img_row['s_img'];
    unlink($old_img_path);

    $sql = "update tb_student set
    s_pass = '$s_pass',
    s_name = '$s_name',
    s_date_of_birth = '$s_date_of_birth',
    s_address = '$s_address',
    s_tel = '$s_tel',
    s_email = '$s_email',
    s_img = '$s_img'
    where s_user = '$s_user'";
    $rs = $conn->query($sql);
    if ($rs) {
        move_uploaded_file($s_img_path, "../student/img/profile/" . $s_img);
        ?>
        <script language="javascript">
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location = "student_acc.php";
        </script>
        <?php
    } else {
        echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
        echo $sql;
        exit();
    }
}
?>