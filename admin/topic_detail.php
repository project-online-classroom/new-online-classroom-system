<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
$w_id = $_GET['w_id'];

include ("connect_db.php");
$sql_view = "select * from tb_webboard where w_id = '$w_id'";
$rs_view = $conn->query($sql_view);
$r_view = $rs_view->fetch_object();
$old_view = $r_view->w_view + 1;

$sql_av = "update tb_webboard set w_view = '$old_view' where w_id = '$w_id'";
$rs_av = $conn->query($sql_av);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<style>
    .main-container {
        width: 600px;
        display: flex;
        flex-direction: column;
        gap: 20px;
        border: 1px solid;
        border-radius: 10px;
        padding: 10px;
        margin-bottom: 30px;
        word-wrap: break-word;
    }

    .main-container-2 {
        background: #D4DADF;
        width: 600px;
        display: flex;
        flex-direction: column;
        gap: 20px;
        border-radius: 10px;
        padding: 10px;
        margin-bottom: 30px;
        word-wrap: break-word;
    }

    .main-container-comment {
        background: #D4DADF;
        width: 600px;
        display: flex;
        flex-direction: column;
        gap: 20px;
        border-radius: 10px;
        padding: 10px;
        margin-bottom: 30px;
        word-wrap: break-word;
    }
</style>

<body>

    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <?php include ("connect_db.php");
    $sql = "select * from tb_webboard where w_id = '$w_id'";
    $rs = $conn->query($sql);
    $r = $rs->fetch_object();
    ?>

    <center>
        <div style="margin-bottom: 10px">
            <h3>รายละเอียดกระทู้</h3>
        </div>
    </center>
    <div class="container text-center main-container">
        <div>
            <img src="../img/webboard/<?= $r->w_img; ?>" alt="" style="height: 300px; width: 300px;">
        </div>
        <div>
            ชื่อกระทู้ : <?= $r->w_topic ?>
        </div>
        <div>
            รายละเอียด : <?= $r->w_detail ?>
        </div>
        <div>
            วันที่โพสต์ : <?= $r->w_date ?>
        </div>
        <div>
            ผู้โพสต์ : <?= $r->w_user ?>
        </div>
        <div>
            สถานะ : <?php if ($r->w_status == 1) {
                echo "ปกติ";
            } else {
                echo "ถูกรายงาน";
            } ?>
        </div>
        <div>
            ยอดวิว : <?= $r->w_view ?> / คอมเมนต์ : <?= $r->w_comment ?>
        </div>

        <div>
            <?php if ($r->w_user == $a_user) { ?>
                <a href="form_update_webboard.php?w_id=<?= $r->w_id; ?>" class="btn btn-warning">
                    แก้ไขกระทู้
                </a>
                <a href="delete_webboard.php?w_id=<?= $r->w_id; ?>" class="btn btn-danger">
                    ลบกระทู้
                </a>
            <?php } else { ?>
                <?php
                $sql_rw = "select * from tb_report_wb where m_user = '$a_user' and w_id = '$w_id'";
                $rs_rw = $conn->query($sql_rw);
                $row_rw = mysqli_num_rows($rs_rw);
                // echo $row_rw; exit();
                if ($row_rw > 0) { ?>
                    <a href="cancle_report_webboard.php?w_id=<?= $r->w_id ?>" class="btn btn-warning" disabled>
                        คุณได้รายงานกระทู้ไปแล้ว || ยกเลิกการรายงาน
                    </a>
                <?php } else {
                    ?>
                    <a href="report_webboard.php?w_id=<?= $r->w_id; ?>" class="btn btn-warning">
                        รายงานกระทู้
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <center>
        <hr width="95%">
    </center>

    <!-- Comment Topic Code -->

    <div class="container text-center main-container-2" style="width: 600px;">
        <div style="margin-bottom: 10px">
            <h3>แสดงความคิดเห็น</h3>
        </div>

        <form name="comment_webboard" action="insert_comment.php" method="post" enctype="multipart/form-data">
            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ความคิดเห็น :</span>
                <input type="text" class="form-control" name="c_comment" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รูปภาพ :</span>
                <input type="file" class="form-control" name="c_img" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default">
            </div>


            <button type="submit" class="btn btn-success">แสดงความคิดเห็น</button>
            <button type="reset" class="btn btn-warning">ยกเลิก</button>
            <input type="hidden" name="w_id" value="<?= $w_id; ?>">
        </form>
    </div>
    <!-- End of Comment Topic Code -->
    <center>
        <hr width="95%">
    </center>
    <div class="container main-container-comment">
        <div style="">
            <h3>ความคิดเห็น</h3>
        </div>

        <?php include ("connect_db.php");
        $sql = "select * from tb_comment where w_id = '$w_id' order by c_date desc";
        $rs = $conn->query($sql);
        while ($r = $rs->fetch_object()) {
            ?>
            <div class="container">
                <div>
                    ความคิดเห็น : <?= $r->c_comment ?>
                </div>
                <div>
                    เวลาที่โพสต์ : <?= $r->c_date ?>
                </div>
                <div>
                    ผู้โพสต์ : <?= $r->c_user ?>
                </div>
                <div style="display: flex; margin-top: 5px; justify-content: space-between; align-items: flex-end;">
                    <?php if ($r->c_img == '') { ?>
                        <img src="../img/comment/no-pictures.png" alt="" width="25px" height="25px">
                    <?php } else { ?>
                        <img src="../img/comment/<?= $r->c_img ?>" alt="" width="200px" height="200px">
                    <?php } ?>
                    <div>
                        <?php if ($r->c_user == $a_user) { ?>
                            <a href="form_update_comment.php?c_id=<?= $r->c_id; ?>" class="btn btn-warning btn-sm">
                                แก้ไข
                            </a>
                            <a href="delete_comment.php?c_id=<?= $r->c_id; ?>&w_id=<?= $r->w_id ?>"
                                class="btn btn-danger btn-sm">
                                ลบ
                            </a>
                        <?php } else { ?>
                            <?php
                            $sql_rc = "select * from tb_report_comment where m_user = '$a_user' and c_id = '$r->c_id'";
                            $rs_rc = $conn->query($sql_rc);
                            $row_rc = mysqli_num_rows($rs_rc);
                            // echo $row_rw; exit();
                            if ($row_rc > 0) { ?>
                                <a href="cancle_report_comment.php?c_id=<?= $r->c_id; ?>&w_id=<?= $r->w_id; ?>"
                                    class="btn btn-warning btn-sm">
                                    ยกเลิกรายงาน
                                </a>
                            <?php } else { ?>
                                <a href="report_comment.php?c_id=<?= $r->c_id; ?>&w_id=<?= $r->w_id ?>"
                                    class="btn btn-warning btn-sm">
                                    รายงาน
                                </a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

                <hr>
            </div>
        <?php } ?>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>