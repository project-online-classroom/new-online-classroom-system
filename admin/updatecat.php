<?php
    include("connect_db.php");

    $c_id = $_POST['c_id'];
    $c_name = $_POST['c_name'];
    $c_img = $_FILES['c_img']['name'];
    $c_img_path = $_FILES['c_img']['tmp_name'];

    if($c_img == '') {
        $sql = "UPDATE tb_category SET c_name = '$c_name' WHERE c_id = '$c_id'";
        
        $rs = $conn->query($sql);
        if($rs) {
?>
            <script language="javascript"> 
                alert("แก้ไขข้อมูลหมวดหมู่สำเร็จ");
                window.location = "category.php";
            </script>
<?php
        } else {
            echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
            echo $sql;  
            exit();
        }
    } else {
        // ลบรูปเก่าออก
        $old_img_sql = "SELECT c_img FROM tb_category WHERE c_id = '$c_id'";
        $old_img_result = $conn->query($old_img_sql);
        $old_img_row = $old_img_result->fetch_assoc();
        $old_img_path = "../img/cat/" . $old_img_row['c_img'];
        unlink($old_img_path);

        // อัปโหลดรูปใหม่
        $sql = "UPDATE tb_category SET
            c_name = '$c_name',
            c_img = '$c_img'
            WHERE c_id = '$c_id'";
        $rs = $conn->query($sql);
        if($rs) {
            move_uploaded_file($c_img_path, "../img/cat/" . $c_img);
?>
            <script language="javascript"> 
                alert("แก้ไขข้อมูลหมวดหมู่สำเร็จ");
                window.location = "category.php";
            </script>
<?php
        } else {
            echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
            echo $sql;
            exit();
        }
    }
?>
