<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>
    <center>
        <div class="container" style="margin-bottom: 20px; width: 60%;">
            <h2 style="margin-bottom:10px;">จัดการบัญชีคุณครู</h2>
            <form class="d-flex" action="" method="post">
                <input class="form-control me-2" type="search" name="txt_s" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </center>
    <div class="container text-center" style="display: flex; flex-wrap: wrap; ">
        <table class="table">
            <?php include ("connect_db.php");
            $sql = "select * from tb_teacher";
            $rs = $conn->query($sql);
            $r = $rs->fetch_object();
            ?>
            <tbody class="table-group-divider">
                <tr align="center">
                    <td colspan="2">
                        <!-- ส่วนของตาราง -->
                        <table class="table table-dark table-striped">
                            <tr align="center">
                                <td colspan="8">
                                    <font face="boonjot" size="5" color="yellow"><b>ข้อมูลนักเรียน</b></font>
                                </td>
                            </tr>
                            <tr style="text-align: center;">
                                <td>
                                    <font face="boonjot" size="5">ลำดับ</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">รหัสบัตรประจำตัวประชาชน</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">ชื่อ - สกุล</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">เพศ</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">เบอร์โทร</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">เพิ่มเติม</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">แก้ไข</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">ลบ</font>
                                </td>
                            </tr>
                            <?php
                            include ("connect_db.php");
                            $txt_s = $_POST['txt_s'];
                            $sql = "select * from tb_teacher
                            where t_name like '%" . $txt_s . "%' or t_id_card like '%" . $txt_s . "%'
                            order by t_id_card asc";
                            $rs = $conn->query($sql);
                            $no = 1;
                            while ($r = $rs->fetch_object()) {
                                ?>
                                <tr>
                                    <td>
                                        <center>
                                            <font face="boonjot" size="5"><?= $no++ ?></font>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <font face="boonjot" size="5"><?= $r->t_id_card; ?></font>
                                        </center>
                                    </td>
                                    <td>
                                        <font face="boonjot" size="5"><?= $r->t_name; ?></font>
                                    </td>
                                    <td>
                                        <center>
                                            <font face="boonjot" size="5"><?php if($r->t_gender == 0) {echo "ชาย";}else{echo "หญิง";} ?> </font>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <font face="boonjot" size="5"><?= $r->t_tel; ?></font>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <a href="profile_teacher.php?t_id_card=<?= $r->t_id_card; ?>"
                                                class="btn btn-info">

                                                <font face="boonjot">ข้อมูลทั้งหมด</font>

                                            </a>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <a href="form_update_profile_teacher.php?t_id_card=<?= $r->t_id_card; ?>"
                                                    class="btn btn-warning">
                                                <font face="boonjot">แก้ไข</font>
                                            </a>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <a href="delete_teacher.php?t_id_card=<?= $r->t_id_card; ?>"
                                                class="btn btn-danger">
                                                <font face="boonjot">ลบ</font>
                                            </a>
                                        </center>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <!-- สิ้นสุดส่วนของตาราง -->
                    </td>
                </tr>

            </tbody>
        </table>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>