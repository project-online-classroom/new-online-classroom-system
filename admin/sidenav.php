<nav class="navbar navbar-dark bg-dark fixed-top" style="background-color: #508bfc;">
    <div class="container-fluid">
        <a class="navbar-brand" href="home.php">Online Classroom System</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar"
            aria-controls="offcanvasDarkNavbar" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div class="offcanvas offcanvas-end text-bg-dark" tabindex="-1" id="offcanvasDarkNavbar"
            aria-labelledby="offcanvasDarkNavbarLabel">
            <div class="offcanvas-header">
                <a class="navbar-brand" href="home.php">
                    <img src="/docs/5.3/assets/brand/bootstrap-logo.svg" width="30" height="24">
                </a>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close">
                </button>
            </div>
            <center>
                <hr width="95%">
            </center>
            <div class="offcanvas-body">

                <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="home.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="teacher_acc.php">จัดการบัญชีคุณครู</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="student_acc.php">จัดการบัญชีนักเรียน</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="category.php">จัดการหมวดหมู่รายวิชา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="manage_subject.php">จัดการรายวิชา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="manage_webboard.php">จัดการกระทู้</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="manage_comment.php">จัดการความคิดเห็น</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Other
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark" style="max-width: fit-content;">
                            <li><a class="dropdown-item" href="profile_admin.php"
                                    style="display: flex; justify-content: center;">Profile</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li>
                                <a class="dropdown-item" href="logout.php">
                                    <button type="button" class="btn btn-outline-danger" style="width: 100%;">Log Out</button>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<br><br><br>
<center><img src="img/header.png" ></center>