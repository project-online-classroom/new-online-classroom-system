<?php
include ("connect_db.php");

$a_user = $_POST['a_user'];
$a_pass = $_POST['a_pass'];
$a_name = $_POST['a_name'];
$a_gender = $_POST['a_gender'];
$a_address = $_POST['a_address'];
$a_tel = $_POST['a_tel'];
$a_img = $_FILES['a_img']['name'];
$a_img_path = $_FILES['a_img']['tmp_name'];

if ($a_img == '') {
    $sql = "update tb_admin set
        a_pass = '$a_pass',
        a_name = '$a_name',
        a_gender = '$a_gender',
        a_address = '$a_address',
        a_tel = '$a_tel'
        where a_user = '$a_user'";
    $rs = $conn->query($sql);
    if ($rs) {
        ?>
        <script language="javascript">
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location = "profile_admin.php";
        </script>
        <?php
    } else {
        echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
        echo $sql;
        exit();
    }
} else {
    // ลบรูปเก่าออก
    $old_img_sql = "SELECT a_img FROM tb_admin WHERE a_user = '$a_user'";
    $old_img_result = $conn->query($old_img_sql);
    $old_img_row = $old_img_result->fetch_assoc();
    $old_img_path = "img/profile/" . $old_img_row['a_img'];
    unlink($old_img_path);

    $sql = "update tb_admin set
    a_pass = '$a_pass',
    a_name = '$a_name',
    a_gender = '$a_gender',
    a_address = '$a_address',
    a_tel = '$a_tel',
    a_img = '$a_img'
    where a_user = '$a_user'";
    $rs = $conn->query($sql);
    if ($rs) {
        move_uploaded_file($a_img_path, "img/profile/" . $a_img);
        ?>
        <script language="javascript">
            alert("แก้ไขข้อมูลสำเร็จ");
            window.location = "profile_admin.php";
        </script>
        <?php
    } else {
        echo "ไม่สามารถแก้ไขข้อมูลได้ครับ";
        echo $sql;
        exit();
    }
}
?>