<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<style>
    .main-container {
        width: 600px;
        display: flex;
        flex-direction: column;
        gap: 20px;
        border: 1px solid;
        border-radius: 5px;
        padding: 10px;
        margin-bottom: 30px;
    }
</style>

<body>

    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <?php include ("connect_db.php");
    $t_id_card = $_GET['t_id_card'];
    $sql = "select * from tb_teacher where t_id_card = '$t_id_card'";
    $rs = $conn->query($sql);
    $r = $rs->fetch_object();
    ?>

    <center>
        <div style="margin-bottom: 10px">
            <h3>ดูข้อมูลเพิ่มเติมสำหรับคุณครู</h3>
        </div>
    </center>
    <div class="container text-center main-container">
        <div>
            <img src="../teacher/img/profile/<?= $r->t_img; ?>" alt=""
                style="height: 200px; width: 200px; border-radius: 50%;">
        </div>
        <div>
            รหัสบัตรประจำตัวประชาชน : <?= $r->t_id_card ?>
        </div>
        <div>
            ชื่อ : <?= $r->t_name ?>
        </div>
        <div>
            วันเกิด : <?= $r->t_date_of_birth ?>
        </div>
        <div>
            เพศ : <?php if($r->t_gender == 0) {echo "ชาย";}else{echo "หญิง";} ?>
        </div>
        <div>
            ที่อยู่ : <?= $r->t_address ?>
        </div>
        <div>
            เบอร์โทรศัพท์ : <?= $r->t_tel ?>
        </div>
        <div>
            Email : <?= $r->t_email ?>
        </div>
        <div>
        วุฒิการศึกษา : <?= $r->t_degree	 ?>
        </div>
        <div>
        ตำแหน่งงาน	 : <?= $r->t_position ?>
        </div>
        <div>
        วันเริ่มงาน	 : <?= $r->t_date_of_hire ?>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>