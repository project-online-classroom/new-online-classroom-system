<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <div class="container text-center" style="width: 600px">
        <div style="margin-bottom: 10px">
            <h3>สร้างกระทู้ของคุณ</h3>
        </div>

        <?php
        include ("connect_db.php");
        $w_id = $_GET['w_id'];
        $sql = "select * from tb_webboard where w_id = '$w_id'";
        $rs = $conn->query($sql);
        $r = $rs->fetch_object();
        ?>

        <form name="fu_webboard" action="update_webboard.php" method="post" enctype="multipart/form-data">
            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รูปภาพ :</span>
                    <input type="file" class="form-control" name="w_img" aria-label="Sizing example input"
                        aria-describedby="inputGroup-sizing-default">
            </div>


            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">หัวข้อกระทู้ :</span>
                <input type="text" class="form-control" name="w_topic" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" required value="<?= $r->w_topic; ?>">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รายละเอียด :</span>
                <textarea class="form-control" name="w_detail" id="floatingTextarea2"
                    style="height: 125px" ><?= $r->w_detail; ?></textarea>
            </div>


            <button type="submit" class="btn btn-success">แก้ไขกระทู้</button>
            <button type="reset" class="btn btn-warning">ยกเลิก</button>
            <input type="hidden" name="w_id" value="<?= $w_id; ?>">
        </form>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>