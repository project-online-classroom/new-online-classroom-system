<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>
    <center>
        <div class="container" style="margin-bottom: 20px; width: 60%;">
            <h2 style="margin-bottom:10px;">จัดการบัญชีนักเรียน</h2>
            <form class="d-flex" action="" method="post">
                <input class="form-control me-2" type="search" name="txt_s" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </center>
    <div class="container text-center" style="display: flex; flex-wrap: wrap; ">
        <table class="table">
            <?php include ("connect_db.php");
            $sql = "select * from tb_student";
            $rs = $conn->query($sql);
            $r = $rs->fetch_object();
            ?>
            <tbody class="table-group-divider">
                <tr align="center">
                    <td colspan="2">
                        <!-- ส่วนของตาราง -->
                        <table class="table table-dark table-striped">
                            <tr align="center">
                                <td colspan="8">
                                    <font face="boonjot" size="5" color="yellow"><b>ข้อมูลนักเรียน</b></font>
                                </td>
                            </tr>
                            <tr style="text-align: center;">
                                <td>
                                    <font face="boonjot" size="5">ลำดับ</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">Username</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">ชื่อ - สกุล</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">เพศ</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">เบอร์โทร</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">เพิ่มเติม</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">แก้ไข</font>
                                </td>
                                <td>
                                    <font face="boonjot" size="5">ลบ</font>
                                </td>
                            </tr>
                            <?php
                            include ("connect_db.php");
                            $txt_s = $_POST['txt_s'];
                            $sql = "select * from tb_student
                            where s_name like '%" . $txt_s . "%' or s_user like '%" . $txt_s . "%'
                            order by s_user asc";
                            $rs = $conn->query($sql);
                            $no = 1;
                            while ($r = $rs->fetch_object()) {
                                ?>
                                <tr>
                                    <td>
                                        <center>
                                            <font face="boonjot" size="5"><?= $no++ ?></font>
                                        </center>
                                    </td>
                                    <td>
                                        <font face="boonjot" size="5"><?= $r->s_user; ?></font>
                                    </td>
                                    <td>
                                        <font face="boonjot" size="5"><?= $r->s_name; ?></font>
                                    </td>
                                    <td>
                                        <center>
                                            <font face="boonjot" size="5"><?= $r->s_gender; ?></font>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <font face="boonjot" size="5"><?= $r->s_tel; ?></font>
                                        </center>
                                    </td>
                                    <td>
                                        <a href="profile_student.php?s_user=<?= $r->s_user; ?>" class="btn btn-info">
                                            <center>
                                                <font face="boonjot" size="4">ข้อมูลทั้งหมด</font>
                                            </center>
                                        </a>
                                    </td>
                                    <td>
                                        <center>
                                            <a href="form_update_profile_student.php?s_user=<?= $r->s_user; ?>"
                                                class="btn btn-warning btn-sm">
                                                <font face="boonjot" size="5">แก้ไข</font>
                                            </a>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <a href="delete_student.php?s_user=<?= $r->s_user; ?>" class="btn btn-danger">
                                                <font face="boonjot" size="5">ลบ</font>
                                            </a>
                                        </center>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <!-- สิ้นสุดส่วนของตาราง -->
                    </td>
                </tr>

            </tbody>
        </table>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>