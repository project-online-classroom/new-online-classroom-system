<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <center>
        <h4>สร้างห้องเรียน</h4>
    </center>
    <div class="container text-center">
        <div style="display: flex; justify-content: center;">
            <div class="col-6">
                <form action="insert_class.php" method="post" enctype="multipart/form-data">
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">รหัสวิชา</span>
                        <input type="text" class="form-control" name="subj_id" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" required>
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">ชื่อห้องเรียน</span>
                        <input type="text" class="form-control" name="subj_name" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" required>
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">รหัสหมวดหมู่</span>
                        <select class="form-select" aria-label="Default select example" name="c_id" required>
                            <option selected>กรุณาเลือกหมวดหมู่</option>
                            <?php
                            include ("connect_db.php");
                            $sql = "select * from tb_category";
                            $rs = $conn->query($sql);
                            while ($r = $rs->fetch_object()) {
                                ?>
                                <option value="<?= $r->c_id; ?>"><?= $r->c_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">รหัสบัตรประชาชน</span>
                        <input type="text" class="form-control" name="t_id_card" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" required>
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">รูปภาพ</span>
                        <input type="file" class="form-control" name="subj_img" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" required> <!-- อัปเดตไม่ต้องใส่ required -->
                    </div>
                    <button type="submit" class="btn btn-success">สร้างห้องเรียน</button>
                    <button type="reset" class="btn btn-warning">ยกเลิก</button>

                </form>
            </div>

        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>