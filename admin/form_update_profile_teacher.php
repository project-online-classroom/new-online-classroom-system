<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<style>
    .main-container {
        width: 600px;
        display: flex;
        flex-direction: column;
        border: 1px solid;
        border-radius: 5px;
        padding: 10px;
        margin-bottom: 30px;
    }
</style>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <div class="container text-center main-container">
        <div style="margin-bottom: 10px">
            <h3>แก้ไขข้อมูลนักเรียน</h3>
        </div>

        <?php
        include ("connect_db.php");
        $t_id_card = $_GET['t_id_card'];
        $sql = "select * from tb_teacher where t_id_card = '$t_id_card'";
        $rs = $conn->query($sql);
        $r = $rs->fetch_object();
        ?>

        <form name="user_update_profile" action="update_profile_teacher.php" method="post" enctype="multipart/form-data">

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ชื่อผู้ใช้งาน</span>
                <input type="text" class="form-control" name="t_id_card" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_id_card; ?>" disabled required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ชื่อนาม-สกุล</span>
                <input type="text" class="form-control" name="t_name" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_name; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">วันเกิด</span>
                <input type="date" class="form-control" name="t_date_of_birth" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_date_of_birth; ?>" required>
            </div>

            <div class="input-group mb-3"
                style="display;gap: 10px;align-items: center;justify-content: space-between;padding-right: 160px;border: 1px solid #dee2e6;border-radius: 8px;">
                <span class="input-group-text" id="inputGroup-sizing-default">
                    เพศ
                </span>
                <div style="display: flex; gap: 10px;">
                    <input type="radio" class="" name="t_gender" value="0" <?php
                    if ($r->t_gender == 0) {
                        echo "checked";
                    }
                    ?> required>ชาย
                    <input type="radio" class="" name="t_gender" value="1" <?php
                    if ($r->t_gender == 1) {
                        echo "checked";
                    }
                    ?> required>หญิง 
                </div>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ที่อยู่</span>
                <input type="text" class="form-control" name="t_address" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_address; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">เบอร์โทรศัพท์</span>
                <input type="text" class="form-control" name="t_tel" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_tel; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">อีเมล</span>
                <input type="email" class="form-control" name="t_email" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_email; ?>" required>
            </div>
            
            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">วุฒิการศึกษา	</span>
                <input type="text" class="form-control" name="t_degree" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_degree; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ตำแหน่งงาน	</span>
                <input type="text" class="form-control" name="t_position" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_position; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">วันเริ่มงาน</span>
                <input type="date" class="form-control" name="t_date_of_hire" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->t_date_of_hire; ?>" required>
            </div>


            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รูปภาพของคุณ :
                    </spanrequired>
                    <input type="file" class="form-control" name="t_img" aria-label="Sizing example input"
                        aria-describedby="inputGroup-sizing-default">
            </div>
            <button type="submit" class="btn btn-success">แก้ไขข้อมูลคุณครู</button>
             <button type="reset" class="btn btn-warning">ยกเลิก</button>
            <input type="hidden" name="t_id_card" value="<?= $t_id_card; ?>">
        </form>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>