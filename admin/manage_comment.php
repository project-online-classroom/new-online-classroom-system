<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>
    <div style="display: flex; flex-direction: column; align-items: center; gap: 20px;">
        <div style="display: flex; gap: 30px;">
            <!-- <a href="#" class="btn btn-light btn-lg">เข้าร่วมห้องเรียน</a>
            <a href="form_insert_class.php" class="btn btn-light btn-lg">สร้างห้องเรียน</a>
            <a href="form_insertcat.php" class="btn btn-light btn-lg">สร้างหมวดหมู่</a> -->
            <!-- <a href="fi_webboard.php" class="btn btn-dark btn-lg">สร้างกระทู้</a> -->
        </div>
    </div>
    <br>

    <div class="container">
        <div class="row align-items-start">
            <div class="col">
                <table class="table table-primary">
                    <thead>
                        <tr align="center">
                            <th scope="col" colspan="9">
                                <font face="boonjot" size="5">จัดการความคิดเห็นที่รายงาน</font>
                            </th>
                        </tr>
                    </thead>

                    <tbody class="table-group-divider">
                        <tr align="center table-info" class="table-info">
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ชื่อกระทู้</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ลำดับ</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">รูปภาพ</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">หัวข้อความคิดเห็น</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ผู้โพสต์</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">วันที่โพสต์</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ความคิดเห็น</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">แก้ไข/รายงาน</font>
                            </td>
                            <td align="center" class="col-sm">
                                <font face="boonjot" size="4">ลบความคิดเห็น</font>
                            </td>
                        </tr>
                        <!-- ตัวแปรแสดงข้อมูลจากฐานข้อมูล -->
                        <?php include ("connect_db.php");
$sql = "SELECT * FROM tb_comment 
inner join tb_report_comment on tb_comment.c_id = tb_report_comment.c_id 
inner join tb_webboard on tb_comment.w_id = tb_webboard.w_id";
$rs = $conn->query($sql);
$no = 1;
while ($r = $rs->fetch_object()) {
    $modal_id = "exampleModalToggle" . $no;
    ?>
    <tr align="center table-info" class="table-info">
        <td align="center" class="col-sm">
            <font face="boonjot" size="4"><?= $r->w_topic ?></font>
        </td>
        <td align="center" class="col-sm">
            <font face="boonjot" size="4"><?= $no++ ?></font>
        </td>
        <td align="center" class="col-sm">
            <?php if ($r->c_img == '') { ?>
                <img src="../img/comment/no-pictures.png" alt="" width="50px" height="50px">
            <?php } else { ?>
                <img src="../img/comment/<?= $r->c_img ?>" alt="" width="50px" height="50px">
            <?php } ?>
        </td>
        <td align="center" class="col-sm">
            <font face="boonjot" size="4"><?= $r->c_comment ?></font>
        </td>
        <td align="center" class="col-sm">
            <font face="boonjot" size="4"><?= $r->c_user ?></font>
        </td>
        <td align="center" class="col-sm">
            <font face="boonjot" size="4"><?= $r->c_date ?></font>
        </td>
        <td align="center" class="col-sm">
            <div class="modal fade" id="<?= $modal_id ?>" aria-hidden="true" aria-labelledby="<?= $modal_id ?>Label" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="<?= $modal_id ?>Label">รายละเอียดความคิดเห็น</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" style="display: flex;flex-direction: column;align-items: flex-center; gap:5px;">
                            <div style="margin-bottom: 10px">
                                <?php if ($r->c_img == '') { ?>
                                    <img src="../img/comment/no-pictures.png" alt="" width="50px" height="50px">
                                <?php } else { ?>
                                    <img src="../img/comment/<?= $r->c_img ?>" alt="" width="50px" height="50px">
                                <?php } ?>
                            </div>
                            <div>
                                รหัสความคิดเห็น : <?= $r->c_id ?>
                            </div>
                            <div>
                                ความคิดเห็น : <?= $r->c_comment ?>
                            </div>
                            <div>
                                สถานะ : <?php if ($r->c_status == 1) {
                                    echo "ปกติ";
                                } else {
                                    echo "ถูกรายงาน";
                                } ?>
                            </div>
                            <div>
                                เวลาที่โพสต์ : <?= $r->c_date ?>
                            </div>
                            <div>
                                ผู้โพสต์ : <?= $r->c_user ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" data-bs-target="#<?= $modal_id ?>" data-bs-toggle="modal">ดูเพิ่มเติม</button>
        </td>
        <td align="center" class="col-sm">
            <a href="form_update_webboard.php?w_id=<?= $r->c_id; ?>" class="btn btn-warning">
                แก้ไข
            </a>
        </td>
        <td align="center" class="col-sm">
            <a href="delete_webboard.php?w_id=<?= $r->c_id; ?>" class="btn btn-danger">
                ลบ
            </a>
        </td>
    </tr>
<?php } ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>