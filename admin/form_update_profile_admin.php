<?php
session_start();
ob_start();
$a_user = $_SESSION['a_user'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Classroom System</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="../global.css">
</head>

<style>
    .main-container {
        width: 600px;
        display: flex;
        flex-direction: column;
        border: 1px solid;
        border-radius: 5px;
        padding: 10px;
        margin-bottom: 30px;
    }
</style>

<body>
    <div style="margin-bottom: 40px"><?php include ("sidenav.php"); ?></div>

    <div class="container text-center main-container">
        <div style="margin-bottom: 10px">
            <h3>แก้ไขข้อมูลแอดมิน</h3>
        </div>

        <?php
        include ("connect_db.php");
        $a_user = $_GET['a_user'];
        $sql = "select * from tb_admin where a_user = '$a_user'";
        $rs = $conn->query($sql);
        $r = $rs->fetch_object();
        ?>

        <form name="user_update_profile" action="update_profile_admin.php" method="post" enctype="multipart/form-data">

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ชื่อผู้ใช้งาน</span>
                <input type="text" class="form-control" name="a_user" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->a_user; ?>" disabled required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รหัสผ่านผู้ใช้งาน</span>
                <input type="password" class="form-control" name="a_pass" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->a_pass; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ชื่อนาม-สกุล</span>
                <input type="text" class="form-control" name="a_name" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->a_name; ?>" required>
            </div>

            <div class="input-group mb-3"
                style="display;gap: 10px;align-items: center;justify-content: space-between;padding-right: 220px;border: 1px solid #dee2e6;border-radius: 8px;">
                <span class="input-group-text" id="inputGroup-sizing-default">
                    เพศ
                </span>
                <div style="display: flex; gap: 10px;">
                    <input type="radio" class="" name="a_gender" value="1" <?php if($r->a_gender==1){echo "checked";}?> required>ชาย
                    <input type="radio" class="" name="a_gender" value="2" <?php if($r->a_gender==2){echo "checked";}?> required>หญิง
                </div>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">ที่อยู่</span>
                <input type="text" class="form-control" name="a_address" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->a_address; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">เบอร์โทรศัพท์</span>
                <input type="text" class="form-control" name="a_tel" aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default" value="<?= $r->a_tel; ?>" required>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">รูปภาพของคุณ :
                    </span>
                    <input type="file" class="form-control" name="a_img" aria-label="Sizing example input"
                        aria-describedby="inputGroup-sizing-default">
            </div>
            <button type="submit" class="btn btn-success">แก้ไขข้อมูลนักเรียน</button>
            <button type="reset" class="btn btn-warning">ยกเลิก</button>
            <input type="hidden" name="a_user" value="<?= $a_user; ?>">
        </form>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>

</html>